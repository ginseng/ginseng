(in-package :ginseng-examples)

(defun http-index (&rest args)
  (declare (ignore args))
  (let* ((packages (list-all-packages))
         list-of-functions)
    (iter (for package in packages)
          (iter (for s in-package package)
                (if (and 
                     (fboundp s)
                     (eq (symbol-package s) package)
                     (equal 0 (search "HTTP-" (symbol-name s))))
                    (push (cons package s) list-of-functions))))
    (with-yaclml-output-to-string
      (<:html
       (<:head
        (<:title "list of apps"))
       (<:body 
        (<:h1 "list of apps")
        (<:ol
         (iter
           (for (package . func) in list-of-functions)
           (let ((s (subseq (symbol-name func) (length "HTTP-"))))
             (<:li
              (<:a :href (concatenate 'string 
                                      ginseng::*ginseng-prefix* "/"
                                      (package-name package) "/" 
                                      s)
                   (<:as-html (concatenate 'string (package-name package) "/" s))))))))))))
      
    
