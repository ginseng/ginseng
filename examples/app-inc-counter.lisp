(in-package :ginseng-examples)
(defun http-inc-counter(&optional (next-action nil))
  (invoke-next-action next-action :main  #'(lambda () (inc-counter-main 0))))
(defun inc-counter-main (counter)
  (yaclml:with-yaclml-output-to-string
    (<:html 
     (<:head
      (<:title "Hello World"))
     (<:body
      (<:h1 "Hello" )
      (<:form :action (dynamic-url (inc-counter-main (1+ counter)))
              (<:p (<:as-html counter))
              (<:input :type "submit" :value "OK"))
      ;; (iter (for (k v) in-hashtable *k*)
      ;;       (<:p (<:as-html k)))
      ))))

(defun http-counter(&optional (next-action nil))
  (invoke-next-action next-action :main #'(lambda () (counter-main 0))))
(defun counter-main (counter)
  (yaclml:with-yaclml-output-to-string
    (<:html 
     (<:head
      (<:title "Hello World"))
     (<:body
      (<:h1 "Hello" )
      (<:p (<:as-html counter))
      (<:br)
      (<:a :href (dynamic-url (counter-main (1+ counter))) "++") 
      (<:as-html "  ")
      (<:a :href (dynamic-url (counter-main (1- counter))) "--")
      ))))
(defun http-add-two-numbers (&optional (next-action nil))
  (invoke-next-action next-action :main #'(lambda () (add-two-numbers-main))))
(defun add-two-numbers-main ()
  (yaclml:with-yaclml-output-to-string
    (<:html 
     (<:head
      (<:title "Add two number"))
     (<:body
      (<:h1 "Please input the first number:")
      (let ((first-number 0))
        (<:form :action (dynamic-url
                          (apply-callbacks)
                          (input-next-number first-number))
                (<:input :type :text
                         :name (with-call-back (v :type 'integer)
                                 (setf first-number v))
                         )))))))
(defun input-next-number(first-number)
  (yaclml:with-yaclml-output-to-string
    (<:html 
     (<:head
      (<:title "Add two number"))
     (<:body
      (<:h1 (<:as-html "Add to " first-number  "."
                       " Please input the second number:"))
      (let ((second-number 0))
        (<:form :action (dynamic-url 
                          (apply-callbacks)
                          (add-the-two-numbers first-number second-number))
                (<:input :type :text
                         :name (bindf second-number 'integer)))
        )))))
(defun add-the-two-numbers ( a b )
  (yaclml:with-yaclml-output-to-string
    (<:html 
     (<:head
      (<:title "Add two number"))
     (<:body
      (<:as-html
       a "+" b "=" (+ a b) )
      (<:br)
      (<:a :href (relative-url-to-app) "try again")
      ))))