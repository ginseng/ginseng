(in-package :ginseng-examples)
(defun http-sum-of(&rest args)           
  (with-html "sum of numbers"
    (let ((a-list-of-number (mapcar #'(lambda (x) 
                                        (or (parse-integer x :junk-allowed t) 0)) 
                                    args)))
      (<:p      
       (<:as-html (format nil "~{~A~^+~}" a-list-of-number) "="
                  (apply #'+ a-list-of-number))))))