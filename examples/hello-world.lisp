(in-package :ginseng-examples)

;; this is the first example of using Ginseng. To create a simple
;; dynamic web page, it is nothing more than a function.
;;
;; for example, if you access 
;;  http://localhost:4242/cgi-bin/ginseng-examples/hello-world/arg1/arg2
;;
;; the function "http-hello-world" in package "ginseng-examples" is
;; invoked with arguments, "arg1" and "arg2", etc. The function must
;; return a string as an HTML page.
;; 
;; "/cgi-bin" is the ginseng prefix, you can change
;; ginseng::*ginseng-prefix* to change the prefix.

(defun http-hello-world(&rest args)
  (declare (ignore args))
  (with-yaclml-output-to-string
    (<:html
     (<:head
      (<:title "Hello World"))
     (<:body 
      (<:h1 "Hello World")))))
(defun http-show-args(&rest args)
  (with-yaclml-output-to-string
    (<:html
     (<:head
      (<:title "Your input arguments"))
     (<:body 
      (<:h1 "Your input arguments:")
      (<:p      
       (<:ol 
        (dolist (arg args)
          (<:li (<:as-html arg)))
        ))))))
