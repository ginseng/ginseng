(in-package :ginseng)

(defvar it nil)
(defmacro aprogn (&rest body)
  `(let ((it (and (boundp 'it) it)))
     ,@(mapcar #'(lambda (s)
                   `(setq it ,s))
               body)
     it))
(defmacro awhen (test &rest body)
  `(let ((it (and (boundp 'it) it)))
     (when (setq it ,test)
       ,@(mapcar #'(lambda (s)
                     `(setq it ,s))
                 body)
       it)))
(defmacro aand (&body body)
  `(let ((it (and (boundp 'it) it)))
     (and
      ,@(mapcar #'(lambda (s)
                     `(setq it ,s))
                 body))))

(let ((output *standard-output*))
  (defun my-debug (&rest args)
    (format output "~&WCY-DEBUG:")
    (apply #'format output args))) 
(defun default-main()
  "
<html>
<head>
<title> GINSENG DEFAULT ACTION FOR DEBUG.</title>
</head>
<body>
<h1> DEFAULT ACTION IS NOT DEFINED </h1>
</body>
")
      
(defvar *last-request* nil)
(defvar *last-reply* nil)
(defvar *last-session* nil)
(defvar *last-app-name* nil)
(defvar *last-k* nil)
(defvar *last-environment* nil)
(defvar *last-acceptor* nil)
(defmacro with-last-environment (&body body)
  `(let* ((hunchentoot::*acceptor* *last-acceptor*)
          (hunchentoot::*request* *last-request*)
          (hunchentoot:*reply* *last-request*)
          (hunchentoot::*acceptor* (hunchentoot:request-acceptor ginseng::*last-request*))
          (hunchentoot::*session* *last-session*)
          (*ginseng-app-name* *last-app-name*)
          (*environment* *last-environment*)
          (*k* *last-k*)
          )
     ,@body))
(defun save-last-environment ()
  (setq *last-acceptor* hunchentoot::*acceptor*
        *last-request* hunchentoot:*request*
        *last-reply* hunchentoot:*reply*
        *last-session* hunchentoot:*session*
        *last-app-name* *ginseng-app-name*
        *last-k* *k*
        *last-environment* *environment*
        ))
(defun clear-package(package)
  (when (not  (packagep package))
    (setq package (find-package package)))
  (dolist (s (let (r) 
               (do-symbols (var package) 
                 (push var r)) r))
    (unintern s package)))
  