(in-package :pattern-match)
(defvar *bindings* nil)
(defun generate-code-null (pattern arg body-cont) 
  (if (null pattern)
      #'(lambda ()
          `(if 'generate-code-null
               (if (null ,arg) ,(funcall body-cont))))))
(defun generate-code-ignore (pattern arg body-cont)
  (declare (ignore arg))
  (if (and (symbolp pattern) 
           (string= (symbol-name pattern) "_"))
      #'(lambda ()
          `(if 'generate-code-ignore
               (progn ,(funcall body-cont))))))
(defun generate-code-T (pattern arg body-cont) 
  (if (eq pattern T)
      #'(lambda ()
          `(if 'generate-code-T
               (if (eq ,arg T) ,(funcall body-cont))))))
(defun generate-code-string (pattern arg body-cont) 
  (if (stringp pattern)
      #'(lambda ()
          `(if 'generate-code-string
               (if (string= ,arg ,pattern) ,(funcall body-cont))))))
(defun generate-code-keywords (pattern arg body-cont) 
  (if (keywordp pattern)
      #'(lambda ()
          `(if 'generate-code-keywords
               (if (eq ,pattern ,arg) ,(funcall body-cont))))))
(defun generate-code-quote (pattern arg body-cont) 
  (if (and (listp pattern)
           (eq (car pattern) 'cl:quote))
      #'(lambda ()
          `(if 'generate-code-quote
               (if (equal ',(cadr pattern) ,arg) ,(funcall body-cont))))))
(defun generate-code-symbol (pattern arg body-cont)
  (if (symbolp pattern)
      #'(lambda ()
          (let ((is-bound (find pattern *bindings*)))
            (if is-bound
                `(if 'generate-code-bound-symbol
                     (if (equal ,pattern ,arg)
                         ,(funcall body-cont)))
                (let  ((*bindings* (cons pattern *bindings*)))
                  `(if 'generate-code-unbound-symbol
                       (let ((,pattern ,arg))
                         (declare (ignorable ,pattern))
                         ,(funcall body-cont)))))))))
(defun generate-code-vector (pattern arg body-cont) 
  (if (vectorp pattern)
      (let ((len (length pattern))
            (vars (map 'vector #'(lambda (v) (declare (ignore v)) (gensym "VEC")) pattern))
            (code-cont body-cont))
        (loop 
            for i downfrom (1- len) to 0
            do (progn
                 (setf code-cont
                       (let ((code-cont code-cont)
                             (i i))
                         #'(lambda ()
                             `(let ((,(aref vars i) (aref ,arg ,i)))
                                         ,(funcall (generate-code (aref pattern i) (aref vars i) code-cont))))))))
        #'(lambda ()
            `(if 'generate-code-vector
                 (if (and (vectorp ,arg)
                          (= (length ,arg) ,len))
                     ,(funcall code-cont)))))))
(defun generate-code-consp (pattern arg body-cont)
  (if (consp pattern)
      (let ((next-car (gensym))
            (next-cdr (gensym)))
        #'(lambda ()
            `(if 'generate-code-consp
                 (if (consp ,arg)
                     (let ((,next-car (car ,arg))
                           (,next-cdr (cdr ,arg)))
                       (declare (ignorable ,next-car ,next-cdr))
                       ,(let ((cdr-code-body-cont (generate-code (cdr pattern) next-cdr body-cont)))
                             (funcall (generate-code (car pattern) next-car cdr-code-body-cont))))))))))
(defun generate-code-default (pattern arg body-cont)
  #'(lambda ()
      `(if 'generate-code-default
           (if (equal ,pattern ,arg)
               ,(funcall body-cont)))))
(defparameter *generate-code-functions*
  (list #'generate-code-null
        #'generate-code-ignore
        #'generate-code-T
        #'generate-code-string
        #'generate-code-keywords
        #'generate-code-quote
        #'generate-code-symbol
        #'generate-code-vector
        #'generate-code-consp
        #'generate-code-default
        ))
(defun generate-code (pattern arg cont)
  "ARG is the formal argument of the generated function. GENERATE-CODE
returns a generated function which matchs the PATTERN against ARG. If
match, the generated function evaluated the BODY and return the
result. "
  (let (next-cont)
    (dolist (f *generate-code-functions*)
      (setf next-cont 
            (or next-cont (funcall f pattern arg cont))))
    (assert next-cont)
    next-cont))

(defun match-helper (expr pattern-body)
  "MATCH evaluate first expression and EXPR is the symbol whose value is the
results of the evaluation. PATTERN-BODY is a list of patterns as follows
  ( (PATTERN_1 BODY_1)
    (PATTERN_2 BODY_2)
    ....
  )

"
  (if (null pattern-body) nil
      (let ((ok (gensym))
            (v (gensym))
            (tmp-args (gensym))
            (pattern (caar pattern-body))
            (body (cdar pattern-body))
            guard)
        (when (and (listp (car body)) (eq (caar body) :guard))
          ;; if the first element of body is :guard, then the next element is
          ;; the GUARD expression.
          (setq guard (cadr (pop body))))
        `(multiple-value-bind (,v ,ok) ;; if pattern is matched, OK is t, V is
             ;; the form which is ready for
             ;; evaluating.
             (funcall
              #'(lambda (,tmp-args) ;; this function return two values which is catched by OK and V.
                  ,(let ((*bindings* nil)) 
                        (funcall 
                         (generate-code pattern tmp-args #'(lambda ()
                                                             (if guard 
                                                                 `(when ,guard (values (progn ,@body) t)) 
                                                                 `(values (progn ,@body) t)))))))
              ,expr)
           (if ,ok ,v
               ,(match-helper expr (cdr pattern-body)))))))
(defmacro match (expr &rest test-body)
  (let ((expr-v (gensym)))
    `(let ((,expr-v ,expr))
       ,(match-helper expr-v test-body))))
(defmacro match-values (multi-values-expr &rest test-body)
  (let ((expr-v (gensym)))
    `(let ((,expr-v (multiple-value-list ,multi-values-expr)))
       ,(match-helper expr-v test-body))))