(in-package :ginseng)
(defparameter *k* nil)
(defparameter *environment* nil)
(defvar *last-k* nil) ;for debug
(defvar *last-environment* nil)
(defun relative-url-to-app (&key 
                            (prefix *ginseng-prefix*)
                            (package (package-name *package*))
                            (app *ginseng-app-name*)
                            (args))
  (format nil "~{~A~^/~}" 
          (append (list prefix package app) args)))
(defmacro dynamic-url (&body body)
  (hunchentoot::with-unique-names
      (next env)
    `(let ((,next (make-unique-id)))
       (setf (gethash ,next *k*)
             (list
              #'(lambda ()
                  ,@body)
              *environment*))
       (relative-url-to-app :args (list ,next)))))
(defun default-main()
  (with-yaclml-output-to-string
    (<:html
     (<:head
      (<:title "GINSENG DEFAULT ACTION FOR DEBUG."))
     (<:body 
      (<:h1 "DEFAULT ACTION IS NOT DEFINED.")
      (<:p 
       (<:table
        (<:tr (<:td "ENVIRONMENT-VARIABLE: ") (<:td " "))
        (maphash 
         #'(lambda (k v)
             (<:tr
              (<:td (<:as-html k))
              (<:td (<:as-html v))))
         *environment*)
        ;; (<:tr (<:td "ARGS: ") (<:td " "))            
        ;; (loop for arg in args
        ;;    for i upfrom 0
        ;;       (<:td (<:as-html i))
        ;;      (<:td (<:as-html arg))
        ;;   )))))
        ))))))
(defun invoke-next-action (next-action 
                           &key
                           (main #'default-main)
                           (init-env-var nil)
                           )
  (let ((*k* (or (session-value 'k) (make-hash-table :test #'equal))))
    (setf (session-value 'k) *k*)
    (setq *last-k* *k*)
    (destructuring-bind (f e1) 
        (gethash next-action *k* 
                 (list main (let ((r (make-hash-table :test #'eq)))
                              (dolist (pair init-env-var)
                                (setf (gethash (car pair) r) (cdr pair)))
                              r)))
      (let ((*environment* (alexandria:copy-hash-table e1)))
        (setq *last-environment* *environment*)
        (funcall f)))))
(defun get-parameter (id type &optional (method :both))
  (hunchentoot::compute-parameter id type method))
(defstruct callback-factory callbacks)
(defun create-callback (cf func
                        &key
                        (type 'string)
                        (call-when-parameter-not-exists nil))
  (let ((id (make-unique-id)))
    (push 
     (if call-when-parameter-not-exists
         #'(lambda ()
             (funcall func (get-parameter id type)))
         #'(lambda ()
              (let ((v (get-parameter id type)))
                (if v (funcall func v)))))
     (callback-factory-callbacks cf))
     id))
(defun apply-callbacks(cf)
  (dolist (f (callback-factory-callbacks cf))
    (funcall f)))

(defmacro with-rebinds(vars &body body)
  `(let (,@(loop for var in vars
              collect `(,var ,var)))
     ,@body))
(defun (setf env-var) (new-value symbol)
  (setf (gethash symbol *environment*) new-value))
(defun env-var (symbol &optional default)
  (gethash symbol *environment* default))