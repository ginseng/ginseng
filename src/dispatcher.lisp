(in-package :ginseng)

(defvar *last-request* nil
  "the last REQUEST object. it is used for debuging and testing")
(defvar *ginseng-acceptor* nil
  "the ACCEPT object")
(defvar *ginseng-prefix* "/cgi-bin"
  "default prefix for the framework.")
(defvar *ginseng-app-name* nil)
(defmacro with-last-environment (&body body)
  `(let* ((ginseng::*last-request* ginseng::*last-request*)
          (hunchentoot::*request* ginseng::*last-request*)
          (hunchentoot::*acceptor* (hunchentoot:request-acceptor ginseng::*last-request*))
          (hunchentoot::*session* (hunchentoot:session-verify ginseng::*last-request*))
          (*environment* *last-environment*)
          (*k* *last-k*))
     ,@body))
(defun ginseng-dispacher ()
  (setq *last-request* *request*)
  (let ((script-name  (script-name*)) )
    (when (search *ginseng-prefix* script-name)
      (aprogn
       (subseq script-name (length *ginseng-prefix*))
       (split-sequence:split-sequence #\/ it) ; split via "/"
       (delete "" it  :test #'string=)  ; delete the empty string.
       (let* ((list it)
              (package-name (nth 0 it))
              (app-name (nth 1 it))
              (args (nthcdr 2 list))
              (package (find-package (string-upcase package-name)))
              (function-symbol (and package 
                                    (find-symbol (string-upcase 
                                                  (concatenate 'string "http-" app-name))
                                                 package)))
              (*package* (or package *package*))
              (*ginseng-app-name* app-name)
              )
         (my-debug "access ~A" script-name)
         (if (fboundp function-symbol)
             (apply function-symbol args)
             (progn 
               (my-debug "APP NOT FIND:~A" (list package-name app-name package function-symbol))
               nil))
         )))))
(let (prefix-dispatcher-func)
  (defun start-server(&key (port 4242))
    (setq *ginseng-acceptor* (make-instance 'acceptor 
                                            :port port))
    (setq hunchentoot:*show-lisp-errors-p* t)
    (push
     (setf prefix-dispatcher-func
           (create-prefix-dispatcher *ginseng-prefix* 'ginseng-dispacher))
     *dispatch-table*)
    (start *ginseng-acceptor*))
  (defun stop-server()
    (stop *ginseng-acceptor*)
    (setq *dispatch-table*
          (remove prefix-dispatcher-func *dispatch-table*))
    (setq *ginseng-acceptor* nil)))
