(defpackage ginseng-system
  (:use :common-lisp :asdf))
(in-package :ginseng-system)
(defsystem "ginseng"
  :description "ginseng: my web framework."
  :version "0.1"
  :author "See the file AUTHORS"
  :licence "See the file LICENSE"
  :depends-on (:hunchentoot
               :iterate
               :split-sequence
               :yaclml
               :alexandria ;; copy-hash-table
               )
  :components
  ((:module src
    :serial t
    :components ((:file "package")
                 (:file "pattern-match" :depends-on ("package"))
                 (:file "my-utils" :depends-on ("package"))
                 (:file "ginseng" :depends-on ("my-utils"))
                 ))))

