(defpackage ginseng-examples-system
  (:use :common-lisp :asdf))
(in-package :ginseng-examples-system)
(defsystem "ginseng-examples"
  :description "ginseng: examples for ginseng."
  :version "0.1"
  :author "See the file AUTHORS"
  :licence "See the file LICENSE"
  :depends-on (:ginseng)
  :components
  ((:module examples
    :serial t
    :components ((:file "package")
                 (:file "examples" :depends-on ("package"))
                 ))))

